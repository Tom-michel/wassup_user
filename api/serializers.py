from rest_framework import serializers
# from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

from rest_framework.authtoken.models import Token
from rest_framework.validators import UniqueTogetherValidator
from .models import Person

User = get_user_model()


class PersonSerializer(serializers.ModelSerializer):
    # user = UserSerializer(many=False)
    class Meta:
        model = Person
        fields = "__all__"
        # fields = ("id", "photo", "created", "updated", "user")

class UserSerializer(serializers.ModelSerializer):
    person = PersonSerializer(required=False)
    class Meta:
        model = User
        # fields = "__all__"
        fields = ("id", "username", "last_name", "email", "password", "person")
    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        Person.objects.create(user=user)
        return user

