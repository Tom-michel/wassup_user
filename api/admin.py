from django.contrib import admin
from .models import Person

# Register your models here.

# class PersonAdmin(admin.ModelAdmin):
#     list_display = ['id', 'phoneNumber', 'user']
# admin.site.register(Person, PersonAdmin)
admin.site.register(Person)