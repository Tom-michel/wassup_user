from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Person(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="person")
    photo = models.ImageField(blank=True, default='person/person.svg')
    created = models.DateTimeField(auto_now_add=True, blank=True)
    updated = models.DateTimeField(auto_now=True, blank=True)
    def __str__(self):
        return f'person - {self.user.username}'